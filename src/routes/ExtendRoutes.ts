import { Router } from "express"
import AppRoutes from "./InterfaceRoutes"

abstract class ExtendRoutes implements AppRoutes {
    public router: Router

    constructor(){
        this.router = Router()
        this.routes()
    }

    abstract routes(): void
}

export default ExtendRoutes