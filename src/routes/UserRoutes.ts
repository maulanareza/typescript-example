import ExtendRoutes from "./ExtendRoutes"
//controller
import UserController from "../controller/UserController2"
import {auth} from '../middlewares/AuthMiddlewares'
import multer from "multer"
const upload = multer()

class userRoutes extends ExtendRoutes {
    public routes(): void {
        this.router.get("/lists", auth ,UserController.lists)
        this.router.get("/info", UserController.info)
        this.router.post("/store", upload.none(), UserController.store)
        this.router.post("/update", upload.none(), UserController.update)
        this.router.post("/delete", upload.none(), UserController.delete)
    }
}

export default new userRoutes().router