function get_user(data: any[]) {
    var rv: any[] = [];
    Object.values(data).forEach(function(key: any, index) {
        rv[index] = {
            "id": key.id,
            "name": key.name,
            "email": key.email,
            "phone": key.phone,
            "created_at": key.createdAt,
            "updated_at": key.updatedAt
        }
    });
    return rv;
}

module.exports = {
    get_user
}