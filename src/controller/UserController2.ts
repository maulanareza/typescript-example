import { Request, Response} from "express"
import Schema from 'async-validator';
const helper = require("../helper/app")
const db = require("../db/models")

class UserController {
    
    lists = async (req: Request, res: Response): Promise<Response> => {
        
        const listUser = await db.user.findAll()
        
        const result = helper.get_user(listUser)
        
        return res.status(200).json({
            code: 200,
            status: 'success',
            message: ['fetch data success.'],
            result: result
        });
    }

    info = async (req: Request, res: Response): Promise<Response> => {

        let { id } = req.query

        const getData = await db.user.findOne({
            where:{
                id:id
            }
        })
        const result = helper.get_user([getData])

        return res.status(200).json({
            code: 200,
            status: 'success',
            message: ['fetch data success.'],
            result: result
        });
    }

    store(req: Request, res: Response):any {
        let { name,email,phone } = req.body
        
        const descriptor:any = {
            name: {
              type: 'string',
              required: true,
            //   validator: (rule:any, value: any) => value === 'muji',
            },
            email: [
            {
              type: 'email',
            },
            { 
                asyncValidator : async (rule:any, value:any, callback:any, source:any, options:any):Promise<any> => {
                  const errors:any[] = []
                  const getData = await db.user.findOne({
                        where:{
                            email:value
                        }
                    })
                    
                    if (getData !== null) {
                        errors.push('email already exists')
                        callback(errors);
                    }
                },
              }
            ],
            phone: {
                type: 'number',
                required: true,
              },
        };
        const validator = new Schema(descriptor);

        // PROMISE USAGE
        validator.validate({name,email,phone:124343}).then(() => {
            return res.status(200).json({
                code: 200,
                status: 'success',
                message:['passed'],
                result: []
            });
        }).catch(({ errors, fields }) => {
            return res.status(200).json({
                code: 401,
                status: 'error',
                message:errors,
                result: []
            });
        });
    }

    update = async (req: Request, res: Response): Promise<Response> => {
        let { id, name,email,phone } = req.body

        await db.user.update({name,email,phone},{
            where: {
                id:id
            }
        })

        const getData = await db.user.findOne({
            where:{
                id:id
            }
        })

        const result = helper.get_user([getData])
        return res.status(200).json({
            code: 200,
            status: 'success',
            message: ['fetch data success.'],
            result: result
        });
    }

    delete = async (req: Request, res: Response): Promise<Response> => {
        
        let { id } = req.body

        await db.user.destroy({
            where:{
                id:id
            }
        })

        return res.status(200).json({
            code: 200,
            status: 'success',
            message: ['delete success.'],
            result: []
        });
    }
}

export default new UserController()