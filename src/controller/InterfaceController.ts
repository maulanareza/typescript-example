import { Request, Response } from "express"

interface AppController {
    lists(req: Request, res: Response): Response
    info(req: Request, res: Response): Response
    store(req: Request, res: Response): Response
    update(req: Request, res: Response): Response
    delete(req: Request, res: Response): Response
}

export default AppController