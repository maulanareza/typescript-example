import { Request, Response} from "express"
import { any } from "sequelize/types/lib/operators";
import Validator from 'validatorjs';
const helper = require("../helper/app")
const db = require("../db/models")

class UserController {
    
    lists = async (req: Request, res: Response): Promise<Response> => {
        
        const listUser = await db.user.findAll()
        
        const result = helper.get_user(listUser)
        
        return res.status(200).json({
            code: 200,
            status: 'success',
            message: ['fetch data success.'],
            result: result
        });
    }

    info = async (req: Request, res: Response): Promise<Response> => {

        let { id } = req.query

        const getData = await db.user.findOne({
            where:{
                id:id
            }
        })
        const result = helper.get_user([getData])

        return res.status(200).json({
            code: 200,
            status: 'success',
            message: ['fetch data success.'],
            result: result
        });
    }

    store(req: Request, res: Response): any  {
        let { name,email,phone } = req.body

        Validator.registerAsync('check_email', async function(email, attribute, req, passes) {
            let data:any[] = await db.user.findOne({
                where: { email: email }
            })
            if (data !== null) {
                passes(false, 'email already exist.');
            }
            else {
                passes();
            }
        });

        const rules: Validator.Rules = {
            name: 'required|min:3',
            email: 'required|email|check_email',
            phone: 'required|numeric'
        }
        const error_msg: Validator.AttributeNames = {
            in: 'invalid :attribute'
        }
        let validation = new Validator(req.body, rules, error_msg);
        validation.checkAsync(passes, fails);

        function fails() {
            let message:any[] = []
            for (var key in validation.errors.all()) {
                var value = validation.errors.all()[key]
                message.push(value[0])
            }
            res.status(200).json({
                code: 401,
                status: 'error',
                message: message,
                result: []
            });
        }

        async function passes() {
            const createdUser = await db.user.create({name,email,phone})

            const result = helper.get_user([createdUser])
            return res.status(200).json({
                code: 200,
                status: 'success',
                message: ['fetch data success.'],
                result: result
            });
        }
    }

    update = async (req: Request, res: Response): Promise<Response> => {
        let { id, name,email,phone } = req.body

        await db.user.update({name,email,phone},{
            where: {
                id:id
            }
        })

        const getData = await db.user.findOne({
            where:{
                id:id
            }
        })

        const result = helper.get_user([getData])
        return res.status(200).json({
            code: 200,
            status: 'success',
            message: ['fetch data success.'],
            result: result
        });
    }

    delete = async (req: Request, res: Response): Promise<Response> => {
        
        let { id } = req.body

        await db.user.destroy({
            where:{
                id:id
            }
        })

        return res.status(200).json({
            code: 200,
            status: 'success',
            message: ['delete success.'],
            result: []
        });
    }
}

export default new UserController()