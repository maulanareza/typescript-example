import { Request, Response, NextFunction } from 'express'
const db = require("../db/models")

export const auth = async ( req: Request, res: Response, next: NextFunction): Promise<any> => {

    const auth = req.headers['authorization']

    if (auth != null) {

        const checkApikey = await db.publisher.findOne({
            where:{
                api_key:auth
            }
        })

        if (checkApikey == null) {
            return res.status(200).json({
                code: 401,
                status: 'error',
                message: ['access denied.'],
                result: []
            })
        }else{
            next()
        }
    } else {
        return res.status(200).json({
            code: 401,
            status: 'error',
            message: ['api_key required.'],
            result: []
        })
    }
}