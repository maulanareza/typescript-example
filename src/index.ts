import express, { Application, Request, Response } from "express"
import bodyParser from "body-parser"
import morgan from "morgan"
import compression from "compression"
import helmet from "helmet"
import cors from "cors"
import { config as dotenv } from "dotenv"
//Router
import userRoutes from "./routes/UserRoutes"

class App {
    public app: Application

    constructor(){
        this.app = express()
        this.parser()
        this.router()
        dotenv()
    }

    protected parser():void {
        this.app.use(bodyParser.json())
        this.app.use(bodyParser.urlencoded({extended: false}))
        this.app.use(morgan("dev"))
        this.app.use(compression())
        this.app.use(helmet())
        this.app.use(cors())
    }

    protected router():void {
        this.app.use("/users", userRoutes)
    }
}

const port: number = 8000
const app = new App().app
app.listen(port, () =>{
    console.log(`Server running on the port ${port}`)
})